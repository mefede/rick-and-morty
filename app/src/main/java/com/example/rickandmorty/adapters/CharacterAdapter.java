package com.example.rickandmorty.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rickandmorty.CharacterDetails;
import com.example.rickandmorty.R;
import com.example.rickandmorty.databinding.CharacterItemBinding;
import com.example.rickandmorty.models.Character;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CharacterAdapter extends RecyclerView.Adapter<CharacterAdapter.ViewHolder> {

    private List<Character> characters;
    private Context context;
    private Intent intent;

    public CharacterAdapter(List<Character> characters, Context context) {
        this.characters = characters;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        CharacterItemBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.character_item, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Character character = characters.get(position);
        String statusAndSpecies = character.status + " - " + character.species;
        holder.binding.setCharacter(character);
        holder.binding.statusSpecies.setText(statusAndSpecies);
        holder.itemView.setOnClickListener(v -> {
            intent = new Intent(context, CharacterDetails.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            String characterJson = new Gson().toJson(character);
            intent.putExtra("character", characterJson);
            context.startActivity(intent);
        });
        holder.binding.statusSpecies.setCompoundDrawablesWithIntrinsicBounds(character.getStatusDrawable(), 0, 0, 0);

        Picasso.with(context).load(character.image).into(holder.binding.characterImage);


    }

    @Override
    public int getItemCount() {
        return characters.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        private CharacterItemBinding binding;

        public ViewHolder(@NonNull CharacterItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

}
