package com.example.rickandmorty.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rickandmorty.R;
import com.example.rickandmorty.databinding.EpisodeItemBinding;
import com.example.rickandmorty.models.Episode;

import java.util.List;

public class EpisodeAdapter extends RecyclerView.Adapter<com.example.rickandmorty.adapters.EpisodeAdapter.ViewHolder> {

    private List<Episode> episodes;

    public EpisodeAdapter(List<Episode> episodes) {
        this.episodes = episodes;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        EpisodeItemBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.episode_item, parent, false);
        return new EpisodeAdapter.ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Episode episode = episodes.get(position);
        holder.binding.setEpisode(episode);
    }

    @Override
    public int getItemCount() {
        return episodes.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        private EpisodeItemBinding binding;

        public ViewHolder(@NonNull EpisodeItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

}