package com.example.rickandmorty;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.rickandmorty.adapters.EpisodeAdapter;
import com.example.rickandmorty.databinding.ActivityCharacterDetailsBinding;
import com.example.rickandmorty.models.Episode;
import com.google.gson.Gson;

import com.example.rickandmorty.models.Character;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import br.com.simplepass.loadingbutton.customViews.CircularProgressButton;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CharacterDetails extends AppCompatActivity implements View.OnClickListener {
    private ActivityCharacterDetailsBinding characterDetailsBinding;
    private Character character;
    private CircularProgressButton loadEpisode;
    private RecyclerView recyclerView;
    private List<Episode> episodes = new ArrayList<>();

    // The character has been created.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        characterDetailsBinding = DataBindingUtil.setContentView(
                this, R.layout.activity_character_details);
        character = new Gson().fromJson(getIntent().getStringExtra("character"), Character.class);
        setView();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        loadEpisode.dispose();
    }

    // The episodes in which the character plays were requested from Api.
    // Necessary UI operations are done.
    private void getEpisodes(List<String> urls) {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<List<Episode>> episodesCall = apiInterface.getCharacterEpisodes(urls);
        episodesCall.enqueue(new Callback<List<Episode>>() {

            @Override
            public void onResponse(Call<List<Episode>> call, Response<List<Episode>> response) {
                assert response.body() != null;
                episodes.addAll(response.body());
                recyclerView.setAdapter(new EpisodeAdapter(episodes));

                loadEpisode.revertAnimation();
                characterDetailsBinding.loadEpisodes.setVisibility(View.GONE);

                recyclerView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<List<Episode>> call, Throwable t) {
                loadEpisode.revertAnimation();
                Toast.makeText(getApplicationContext(), R.string.error_occurred, Toast.LENGTH_SHORT).show();
            }
        });
    }

    // UI values are filled in.
    private void setView() {
        recyclerView = characterDetailsBinding.episodeList;
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        characterDetailsBinding.setCharacter(character);
        loadEpisode = characterDetailsBinding.loadEpisodes;
        loadEpisode.setOnClickListener(this);
        characterDetailsBinding.cStatus.setCompoundDrawablesWithIntrinsicBounds(character.getStatusDrawable(), 0, 0, 0);
        Picasso.with(this).load(character.image).into(characterDetailsBinding.characterImg);
    }

    // The ids of the episodes where the charcter is located were determined.
    private List<String> getEpisodesUrl() {
        List<String> urls = new ArrayList<>();
        for (String url : character.episode) {
            String[] items = url.split("/");
            if (items.length > 0)
                urls.add(items[items.length - 1]);
        }
        return urls;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == loadEpisode.getId()) {
            loadEpisode.startAnimation();
            getEpisodes(getEpisodesUrl());
        }
    }
}