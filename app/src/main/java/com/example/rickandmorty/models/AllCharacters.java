package com.example.rickandmorty.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AllCharacters {

    @SerializedName("info")
    @Expose
    public Info info;
    @SerializedName("results")
    @Expose
    public List<Character> results = null;

}
