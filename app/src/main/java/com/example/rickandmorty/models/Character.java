package com.example.rickandmorty.models;

import com.example.rickandmorty.R;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

public class Character {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("species")
    @Expose
    public String species;
    @SerializedName("type")
    @Expose
    public String type;
    @SerializedName("gender")
    @Expose
    public String gender;
    @SerializedName("origin")
    @Expose
    public Origin origin;
    @SerializedName("location")
    @Expose
    public Location location;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("episode")
    @Expose
    public List<String> episode = null;
    @SerializedName("url")
    @Expose
    public String url;
    @SerializedName("created")
    @Expose
    public Date created;

    public int getStatusDrawable() {
        switch (status) {
            case "Alive": return R.drawable.ic_baseline_point_alive;
            case "Dead": return R.drawable.ic_baseline_point_dead;
            default: return R.drawable.ic_baseline_point;
        }
    }

}
