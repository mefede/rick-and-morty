package com.example.rickandmorty;

import com.example.rickandmorty.models.AllCharacters;
import com.example.rickandmorty.models.Episode;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {
    // The link to get the characters has been created.
    @GET("character/")
    Call<AllCharacters> getAllCharacters(@Query("page") String page);

    // The link to get the episodes has been created.
    @GET("episode/{episodes}")
    Call<List<Episode>> getCharacterEpisodes(@Path("episodes") List<String> episodes);
}
