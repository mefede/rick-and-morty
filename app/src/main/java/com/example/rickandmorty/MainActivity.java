package com.example.rickandmorty;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.rickandmorty.adapters.CharacterAdapter;
import com.example.rickandmorty.databinding.ActivityMainBinding;
import com.example.rickandmorty.models.AllCharacters;
import com.example.rickandmorty.models.Character;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView recyclerView;
    private LinearLayout progressView;
    private String next;
    private String prev;
    private Button nextButton;
    private Button prevButton;
    private FloatingActionButton reloadButton;
    private ProgressBar progressBar;
    private boolean isFirst = true;
    private  ActivityMainBinding mainBinding;
    private LinearLayout reloadLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainBinding = DataBindingUtil.setContentView(
                this, R.layout.activity_main);

        nextButton = mainBinding.nextButton;
        nextButton.setOnClickListener(this);
        prevButton = mainBinding.prevButton;
        prevButton.setOnClickListener(this);
        reloadButton = mainBinding.reloadButton;
        reloadButton.setOnClickListener(this);
        progressBar = mainBinding.progressBar;
        reloadLayout = mainBinding.reloadLayout;
        recyclerView = mainBinding.charactersList;
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        progressView = mainBinding.progressLoader;
        getCharacters("1");
    }

    // A request to get the characters has been sent to the API.
    private void getCharacters(String page) {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AllCharacters> charactersCall = apiInterface.getAllCharacters(page);
        int beforeResponseNextStatus = nextButton.getVisibility();
        int beforeResponsePrevStatus = prevButton.getVisibility();
        charactersCall.enqueue(new Callback<AllCharacters>() {

            //If characters were successfully retrieved, they were displayed in ui.
            @Override
            public void onResponse(Call<AllCharacters> call, Response<AllCharacters> response) {
                if (response.body() != null) {
                    setView(response);
                }
                progressView.setVisibility(View.GONE);
                mainBinding.prevNextLayout.setVisibility(View.VISIBLE);
            }

            //If the API cannot be reached, the necessary ui operations have been performed.
            // And warnings were shown.
            @Override
            public void onFailure(Call<AllCharacters> call, Throwable t) {
                if (isFirst) {
                    recyclerView.setVisibility(View.GONE);
                    progressBar.setVisibility(View.GONE);
                    reloadLayout.setVisibility(View.VISIBLE);
                } else {
                    nextButton.setVisibility(beforeResponseNextStatus);
                    prevButton.setVisibility(beforeResponsePrevStatus);
                    Toast.makeText(getApplicationContext(), R.string.error_occurred, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    // The display operations of the characters have been carried out.
    // Actions of Next and Prev buttons were created.
    private void setView(Response<AllCharacters> response) {
        isFirst = false;
        assert response.body() != null;
        List<Character> characters = new ArrayList<>(response.body().results);
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setAdapter(new CharacterAdapter(characters, getApplicationContext()));

        if (response.body().info.next != null) {
            String[] items = response.body().info.next.split("page=");
            if (items.length > 0) {
                next = items[items.length - 1];
                nextButton.setVisibility(View.VISIBLE);
            }
        } else {
            nextButton.setVisibility(View.GONE);
        }

        if (response.body().info.prev != null) {
            String[] items = response.body().info.prev.split("page=");
            if (items.length > 0) {
                prev = items[items.length - 1];
                prevButton.setVisibility(View.VISIBLE);
            }
        } else {
            prevButton.setVisibility(View.GONE);
        }
    }


    // Button click operations were performed.
    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == nextButton.getId()) {
            getCharacters(next);
        } else if (id == prevButton.getId()) {
            getCharacters(prev);
        } else if (id == reloadButton.getId()){
            progressBar.setVisibility(View.VISIBLE);
            reloadLayout.setVisibility(View.GONE);
            getCharacters("1");
        }
    }
}